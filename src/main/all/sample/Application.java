package sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Jsr330ScopeMetadataResolver;

/**
 * jp.co.sample.sample.Application.java
 *
 * SpringBootアプリケーションの起動
 *
 * @since 1.0
 * @author ch16a
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(
        // 性能試験用にdefault @scope("prototype")
        scopeResolver = Jsr330ScopeMetadataResolver.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
