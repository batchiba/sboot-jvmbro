package sample.api.java;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * JavaApiサンプル
 *
 * @author ch16a
 */
@RestController
@RequestMapping("java")
public class SampleJavaApi {

    @RequestMapping(value = "sample", method = RequestMethod.POST)
    public List sampleApi() {
        List<String> list = new ArrayList<String>();
        for(int i = 0; i < 100; i++) {
            list.add(i + "");
        }
        return list;
    }
}
